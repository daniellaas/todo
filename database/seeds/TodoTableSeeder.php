<?php

use Illuminate\Database\Seeder;

class TodoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('todos')->insert(
        [
            [
            'title' => 'Buy milk',
            'use_id' => 1,
            'created_at' => date('Y-m-d G:i:s'),
            ],
            [
            'title' => 'prepare for a test',
            'use_id' => 1,
            'created_at' => date('Y-m-d G:i:s'),
            ],
            [
            'title' => 'cook dinner',
            'use_id' => 2,
            'created_at' => date('Y-m-d G:i:s'),
            ],
        ]);
    }
}
